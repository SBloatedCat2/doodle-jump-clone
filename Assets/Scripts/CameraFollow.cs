using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform followTransform;
    public float maxHeightY=0;
    private Camera cam;
    public GameObject platformDeathCollider;
    private void Start()
    {
        cam =Camera.main;
    }

    // Update is called once per frame
    private void Update()
    {
        platformDeathCollider.transform.position = cam.ScreenToWorldPoint(new Vector3(440, -20, 10));
    }
    void FixedUpdate()
    {
        if (followTransform.position.y > maxHeightY)
        {
            this.transform.position = new Vector3(this.transform.position.x, followTransform.position.y, this.transform.position.z);
            maxHeightY = followTransform.position.y;
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x, maxHeightY, this.transform.position.z);
        }
    }
}
