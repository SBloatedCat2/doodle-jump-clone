using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : MonoBehaviour
{
    [SerializeField] int boostTime = 10;
    [SerializeField] int boostVolume = 10;
    // Start is called before the first frame update
    public GameObject parentPlatform;
    void Start()
    {
        //Make consumable a child of parent platform!!!
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
            collision.GetComponentInParent<PlayerMovement>().getBoosted(boostTime,boostVolume );
         
            print($"Player boosted for {boostTime} seconds with {boostVolume} unit per second");
        }
    }

}
