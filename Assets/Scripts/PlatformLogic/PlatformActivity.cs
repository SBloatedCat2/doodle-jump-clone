
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformActivity : MonoBehaviour
{
    [SerializeField] private float jumpForce=10;
    [SerializeField] private float widthCoeff = 1.0f;
   // [SerializeField] private float heightCoeff = 1.0f;
 //   [SerializeField] private float speed = 0.01f;
    private float screenHalfHeight;
    private float screenHalfWidth;

    public delegate void OnTeleport(GameObject platform);
    public static OnTeleport onLowerBoundary;
    void Start()
    {
        screenHalfHeight = Camera.main.orthographicSize;
        screenHalfWidth = Camera.main.orthographicSize * Screen.width / Screen.height;
     //   points = generateBoundaries();
        // print($" {points[0]} � {points[1]} �����");
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "DeathTrigger")
        {
            respawnPlatform();
            onLowerBoundary?.Invoke(this.gameObject);
         //   print($"yelp{this.gameObject.GetComponentInChildren<Transform>()}");
        }

    }
        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.y <= 1)
                {
                    collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
                }

            }
        }

        void respawnPlatform()
        {
            float randWidth = Random.Range(-screenHalfWidth * widthCoeff, screenHalfWidth * widthCoeff);
          //  float randHeight = Random.Range(0, screenHalfHeight * heightCoeff);
            transform.position = new Vector3(randWidth, transform.position.y + screenHalfHeight * 2, 0);
            //   togglePlatformAbility();
        }
    private void OnDestroy()
    {
        print("Destroyed by PlatformActivity");
    }
    private void OnEnable()
    {
        print("Enabled by PlatformActivity");
    }
}
