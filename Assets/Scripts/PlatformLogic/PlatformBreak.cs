using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBreak : MonoBehaviour
{
    [SerializeField] private GameObject brokenVersion;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<Rigidbody2D>().velocity.y <= 1)
            {
                Instantiate(brokenVersion,this.transform.position,Quaternion.identity);

                foreach (SpriteRenderer go in this.gameObject.GetComponentsInChildren<SpriteRenderer>())
                {
                    go.enabled = false;
                }
            }
            
        }
    }
}
