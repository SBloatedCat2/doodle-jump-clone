using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{
    float screenHalfWidth;
    float speed = 0.01f;
    float[] points = new float[2];
    // Start is called before the first frame update
    void Start()
    {
        screenHalfWidth = Camera.main.orthographicSize * Screen.width / Screen.height;
        points = generateBoundaries();

        this.transform.position = new Vector2(points[0], this.transform.position.y);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        this.transform.position = new Vector2(this.transform.position.x + speed, this.transform.position.y);
        if (this.transform.position.x > points[1]|| this.transform.position.x < points[0])
        {
            speed = speed * -1;
        }

    }

    float[] generateBoundaries()
    {
        points[0] = Random.Range(-screenHalfWidth, screenHalfWidth);
        points[1] = Random.Range(-screenHalfWidth, screenHalfWidth);
        System.Array.Sort(points);
        return points;
    }

}
