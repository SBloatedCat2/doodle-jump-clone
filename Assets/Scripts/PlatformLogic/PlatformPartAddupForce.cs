using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPartAddupForce : MonoBehaviour
{
    float magnitude = 1.0f;
    float rotMagnitude = 100.0f;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(Random.value, Random.value, 0) * magnitude;
        // this.transform.Rotate(new Vector3(Random.value,0 , 0) * rotMagnitude);
        this.GetComponent<Rigidbody2D>().AddTorque((Random.value-0.5f)*rotMagnitude);
        Destroy(this.gameObject,1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
