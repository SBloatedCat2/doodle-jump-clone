using System.Collections;
using UnityEditor;
using UnityEngine;

public class PlatformSpawn : MonoBehaviour
{
   // public GameObject platformPrefab;
  //  public GameObject movingPlatform;
    public GameObject[] platformTypes;
    public GameObject[] consumableTypes;
    public GameObject enemyPrefab;
    public Transform playerTransform;
    public float widthCoeff = 1.0f;
    public float heightCoeff = 1.0f;
    float screenHalfHeight;
    float screenHalfWidth;
    float randTime;
    void Start()
    {
        screenHalfHeight = Camera.main.orthographicSize;
        screenHalfWidth = Camera.main.orthographicSize * Screen.width / Screen.height;
        SpawnNextPlatformChunk(8, platformTypes[0]);
        randTime = Random.Range(30, 60);
        PlatformActivity.onLowerBoundary = onMovePlatform;
    }
    private void FixedUpdate()
    {
   //     printPlatformCount();

    }
    void onMovePlatform(GameObject platform)
    {
        GameObject spawnedPlatform;
       // Destroy(platform.GetComponent<PlatformMove>());
        if (Random.Range(0, 100) > 75)//moving
        {
            spawnedPlatform = Instantiate(platformTypes[1], platform.transform.position, Quaternion.identity);
        }
        else if(Random.Range(0, 100) > 75)//breakable
        {
            spawnedPlatform = Instantiate(platformTypes[2], platform.transform.position, Quaternion.identity);
        }
        else//default
        {
            spawnedPlatform = Instantiate(platformTypes[0], platform.transform.position, Quaternion.identity);
        }
        Destroy(platform);
        checkSpawnConsumable(0, spawnedPlatform.transform);
    }

    void checkSpawnConsumable(int type, Transform spawnedPlatform)
    {
        if (Random.Range(0, 100) > 90)
        {
            Instantiate(consumableTypes[type], spawnedPlatform.transform.position, Quaternion.identity, spawnedPlatform.transform);
        }
    }
    void printPlatformCount()
    {
        print(GameObject.FindGameObjectsWithTag("Platform").Length);
    }
    void Update()
    {
        
        if (Time.time - randTime >0) {
            float posX = Random.Range(-screenHalfWidth * widthCoeff, screenHalfWidth * widthCoeff);
            Instantiate(enemyPrefab, new Vector3(posX, Camera.main.transform.position.y+ screenHalfHeight, 0), Quaternion.identity);
            randTime = Random.Range(0, 5)+ Time.time;
        }
    }
    void SpawnNextPlatformChunk(int platformCount,GameObject pref)
    {
        for(int i=0;i<platformCount; i++)
        {
            /*float randWidth = 
            float randHeight = Random.Range(-screenHalfHeight, screenHalfHeight*heightCoeff);*/
            float posX = Random.Range(-screenHalfWidth * widthCoeff, screenHalfWidth * widthCoeff);
            float posY = screenHalfHeight*2*(i+1)/platformCount;
            Instantiate(pref, new Vector3(posX, posY, 0), Quaternion.identity);
        }
        
    }
}
