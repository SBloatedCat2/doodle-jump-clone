using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private float projectileVelocity = 30.0f;
    [SerializeField] private float cooldown = 1.0f;

    private Rigidbody2D playerBody;
    private float lastShotTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        playerBody = GameObject.Find("Player").GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            shootProjectile();
        }
    }
    void shootProjectile()
    {

        if (Time.time - lastShotTime < cooldown)
        {
            return;
        }
        else
        {
            lastShotTime = Time.time;
            Vector3 mouseClickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 dump = playerBody.velocity;
            mouseClickPos.z = 20;
            mouseClickPos.x = mouseClickPos.x * 10;
            GameObject projectile = Instantiate(projectilePrefab, playerBody.position, Quaternion.identity);
            projectile.GetComponent<Rigidbody2D>().velocity = (mouseClickPos + dump).normalized * projectileVelocity;
        }
    }
}
