using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D body;
    private Transform cam;

    private float deathHeight = 0;
    private float playerHeight = 0;
    private float screenUnitWidth = 0;

    [SerializeField] private float shiftSpeed = 10.0f;
    [SerializeField] private GameObject deathPromt;
    [SerializeField] private GameObject restartButton;
    [SerializeField] private GameObject mainMenuButton;

    void killPlayer()
    {
        Time.timeScale = 0.0f;
        deathPromt.SetActive(true);
        restartButton.SetActive(true);
        mainMenuButton.SetActive(true);
    }

    IEnumerator Boost(float timeSeconds,int velocity)
    {
        float timeFrames=timeSeconds/Time.deltaTime;
        for (int i = 0; i < timeFrames; i++) {
            body.velocity = new Vector2(0,velocity);
          //  print($"{i} coroutine tick");
            yield return null;
        }
        
    }
    public void getBoosted(float timeSeconds,int velocity)
    {
        StartCoroutine( Boost(timeSeconds,velocity));
    }
    void Start()
    {
        screenUnitWidth = Camera.main.orthographicSize * Screen.width / Screen.height;
        deathHeight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0))[1];

        body =GetComponent<Rigidbody2D>();
        cam = GameObject.Find("Main Camera").transform;
       // StartCoroutine(Boost(5,10));

    }

    void FixedUpdate()
    {
        playerHeight = Mathf.Abs(cam.position.y) - Mathf.Abs(body.position.y);
        if (Input.GetKey(KeyCode.A))
        {
            body.velocity = new Vector2(-shiftSpeed, body.velocity.y);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z); // Mirror to the left
        }
        else if (Input.GetKey(KeyCode.D))
        {
            body.velocity = new Vector2(shiftSpeed, body.velocity.y); 
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z); // Mirror to the right
        }
        else body.velocity = new Vector2(0, body.velocity.y);

        if (Mathf.Abs(playerHeight) > Mathf.Abs(deathHeight))
        {
            killPlayer();
        }
        if (Mathf.Abs(body.position.x) > screenUnitWidth)
        {
            body.position = new Vector3(-body.position.x, body.position.y,0);
        }


    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            killPlayer();
        }
    }

}
