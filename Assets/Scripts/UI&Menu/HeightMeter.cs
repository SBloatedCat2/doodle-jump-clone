using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HeightMeter : MonoBehaviour
{

    public TextMeshProUGUI counterText;
    public TextMeshProUGUI screenCountsText;
    public float counter;
    public Transform cameraRef;
    public void Update()
    {
        counterText.text = Mathf.Round(cameraRef.position.y).ToString();
    }

}
