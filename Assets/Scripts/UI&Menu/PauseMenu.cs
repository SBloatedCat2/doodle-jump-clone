using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pausePromt;
    [SerializeField] private GameObject menuButton;
    private bool isPause=false;
    void Update()
    {
        if(!isPause)
        { 
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                pausePromt.SetActive(true);
                menuButton.SetActive(true);
                Time.timeScale = 0.0f;
                isPause=true;
            }
        }
        else 
        {
            if(Input.GetKeyDown(KeyCode.Escape)) 
            {
            pausePromt.SetActive(false);
            menuButton.SetActive(false);
            Time.timeScale = 1.0f;
            isPause = false;
            }
        }
    }
}
